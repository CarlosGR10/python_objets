class coche():        #Clase
    largoChasis=250   #propiedades
    anchoCHasis=120
    ruedas=4
    marcha=False

    def arrancar(self): #metodo (Son funciones dentro de una clase)
        self.marcha=True
    
    def estado(self):      #Comportamiento (arrancar y estado)
        if(self.marcha):
            return "El coche esta en marcha"
        else:
            return "El coche esta parado"


miCoche=coche()    #Instanciar una clase
print(miCoche.largoChasis)
print(miCoche.ruedas)

miCoche.arrancar() #Instanciar un metodo

print(miCoche.estado()) 