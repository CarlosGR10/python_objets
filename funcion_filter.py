# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 20:42:36 2019

@author: Carlos García
"""

#Función Filter
'''Verifica elementos de una secuencia cumplan una condición, devolvinedo un interador
 con los elementos que cumplen dicha condición'''
 
#Ejemplo1
def numero_par(num):
    if num % 2 == 0:
        return True
     
     
numeros=[17,24,18,15,56,80,90,100]

listaNumeros = list(filter(numero_par, numeros))

print(listaNumeros)


#Simplificar con lambda --------------------->>>

numbers=[2,3,4,5,6,7,8,9,10,12,18,20,35,40]

listNumbers = list(filter(lambda num_par: num_par % 2 == 0 , numbers))

print(listNumbers)


#Ejemplo2 Filtrar Objetos
class Empleado:
    
    def __init__(self,nombre,cargo,salario):
        
        self.nombre = nombre
        
        self.cargo = cargo
        
        self.salario = salario
        
    def __str__(self):
        
        return "{} que trabaja como {} tiene un salario de {} $".format(self.nombre,self.cargo,self.salario)
        
        
listaEmpleados = [
        
        Empleado("Juan","Director",25000),
        Empleado("Pedro","Presidente",820000),
        Empleado("Ana","Administrativo",18000),
        Empleado("Pablo","Secretario",15000),
        Empleado("Victor","Ingeniero",20000),
]

#Determinar los salarios que son arriba de 20000

salarios_altos = filter(lambda trabajo: trabajo.salario > 20000,listaEmpleados)

for empleado_salario in salarios_altos:
    print(empleado_salario)




