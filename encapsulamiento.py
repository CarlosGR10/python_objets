class coche():

    def __init__(self): #Constructor

        self.__largoChasis=250   #propiedades
        self.__anchoCHasis=120
        self.__ruedas=4       #encapsulamiento es con dos guines bajos
        self.__marcha=False

    def arrancar(self,run): #metodo (Son funciones dentro de una clase) #Parametros entre parentesis
        self.__marcha=run

        if(self.__marcha):
            chequeo=self.__chequeo_interno()
        
        if(self.__marcha and chequeo):
            return "El coche esta en marcha"
        
        elif(self.__marcha and chequeo==False):
            return "Algo anda mal en el chequeo. No podemos arrancar"

        else:
            return "El coche esta parado"

    def estado(self):      #Comportamiento (arrancar y estado)
       print("El coche tiene, Ruedas: ",self.__ruedas,",Ancho de chasis: ",self.__anchoCHasis,"Largo de chasis: ",self.__largoChasis)

    def __chequeo_interno(self): #Encapsulamiento de metodos
        print("Realizando el chequeo interno")

        self.gasolina="ok"
        self.aceite="ok"
        self.puertas="Cerradas"

        if(self.gasolina=="ok" and self.aceite=="ok" and self.puertas=="Cerradas"):
            return True
        else:
            return False

print("<----------------------Este es el primer objeto------------------------->")

miCoche=coche()    #Instanciar una clase

print(miCoche.arrancar(True)) #Instanciar un metodo

print(miCoche.estado())

print("<--------------------Creamos un segundo objeto------------------------->")

miCoche2=coche()

print(miCoche2.arrancar(False))

print(miCoche2.estado())