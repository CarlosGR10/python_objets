class coche():

    def __init__(self): #Constructor (Se define con un init)

        self.largoChasis=250   #propiedades
        self.anchoCHasis=120
        self.__ruedas=4       #encapsulamiento es con dos guines bajos
        self.marcha=False

    def arrancar(self,run): #metodo (Son funciones dentro de una clase) #Parametros entre parentesis
        self.marcha=run
        
        if(self.marcha):
            return "El coche esta en marcha"
        else:
            return "El coche esta parado"

    def estado(self):      #Comportamiento (arrancar y estado)
       print("El cocche tiene, Ruedas: ",self.__ruedas,",Ancho de chasis: ",self.anchoCHasis,"Largo de chasis: ",self.largoChasis)

print("<----------------------Este es el primer objeto------------------------->")
miCoche=coche()    #Instanciar una clase

print(miCoche.arrancar(True)) #Instanciar un metodo

print(miCoche.estado())

print("<--------------------Creamos un segundo objeto------------------------->")

miCoche2=coche()

print(miCoche2.arrancar(False))

print(miCoche2.estado())