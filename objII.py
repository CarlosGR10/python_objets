class coche():        #Clase
    largoChasis=250   #propiedades
    anchoCHasis=120
    ruedas=4
    marcha=False

    def arrancar(self,run): #metodo (Son funciones dentro de una clase) #Parametros entre parentesis
        self.marcha=run
        
        if(self.marcha):
            return "El coche esta en marcha"
        else:
            return "El coche esta parado"

    def estado(self):      #Comportamiento (arrancar y estado)
       print("El cocche tiene, Ruedas: ",self.ruedas,",Ancho de chasis: ",self.anchoCHasis,"Largo de chasis: ",self.largoChasis)

print("<----------------------este es el primer objeto------------------------->")
miCoche=coche()    #Instanciar una clase
print(miCoche.largoChasis)
print(miCoche.ruedas)

print(miCoche.arrancar(True)) #Instanciar un metodo

print(miCoche.estado())

print("<--------------------Creamos un segundo objeto------------------------->")

miCoche2=coche()
print(miCoche2.largoChasis)
print(miCoche2.ruedas)

print(miCoche2.arrancar(False))

print(miCoche2.estado())