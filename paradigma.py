#Programacion orientado a objetos

class Person:

    def __init__(self,name,age): #Self hace referencia a la classe, autoreferencia
        self.name = name
        self.age = age

    def say_hello(self):
        print('Hello, you name is {} and your age is {} years old'.format(self.name,self.age))

if __name__ == '__main__':

    person1 = Person('David',34)

    print('Age: {}'.format(person1.age))

    person1.say_hello()