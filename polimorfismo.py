class carro():
    def dezplazamiento(self):
        print("me dezplazo en 4 ruedas")

class moto():
    def dezplazamiento(self):
        print("me dezplazo en 2 ruedas")


class camion():
    def dezplazamiento(self):
        print("me dezplazo en 6 ruedas")

miVehiculo1=carro()
miVehiculo1.dezplazamiento()

miVehiculo2=moto()
miVehiculo2.dezplazamiento()

miVehiculo3=camion()
miVehiculo3.dezplazamiento()

def desplazamientoVehiculo(vehiculo): #Polimorfismo, es crear un función que llame a los metodos
    vehiculo.dezplazamiento() #crear un objeto que llame a los metodos

miVehiculo_polimorfico=carro() # LLmama a cualquier clase (carro,moto u camion)
desplazamientoVehiculo(miVehiculo_polimorfico) 
