# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 20:17:40 2019

@author: Carlos García
"""

#Funciones Lambda
#No bucles, ni condicionales, solo calculos y funciones sencillas

'''def area_triangulo(base,altura):  #Funcion Normal
    
    return (base*altura)/2

tria1=area_triangulo(5,7)

tria2=area_triangulo(10,8)

print(tria1)
print(tria2)'''

#Ejemplo1

''' NombreFuncion = lambda parametros: Calculo'''

area_triangulo = lambda base,altura:(base*altura)/2
#Los dos puntos es como un return

tria1=area_triangulo(5,7)

tria2=area_triangulo(10,8)

print("Tria 1 = {}".format(tria1))

print("Tria 2 = {}".format(tria2))

#Ejemplo2

al_cubo=lambda numero:pow(numero,3)
potencia=lambda number:number**3

cubo1 = al_cubo(2)
cubo2 = potencia(3)

print("Al cubo = {}".format(cubo1))
print("Potencia = {}".format(cubo2))

#Ejemplo3

destacar_valor = lambda comision:"¡{} $!".format(comision)

comision_ana = destacar_valor(1000)

print(comision_ana)