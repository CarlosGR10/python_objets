class Vehiculos():
    def __init__(self, marca, modelo):
        self.marca=marca
        self.modelo=modelo
        self.enmarcha=False
        self.acelera=False
        self.frena=False

    def arrancar(self):
        self.enmarcha=True

    def acelerar(self):
        self.acelera=True

    def frenar(self):
        self.frena=True

    def estado(self):
        print("<<------------------>>")
        print(" Marca:",self.marca,"\n Modelo:",self.modelo,"\n En marcha",self.enmarcha,"\n Acelera:",
        self.acelera,"\n Frena:",self.frena)

class Moto(Vehiculos):
    pass
miMoto=Moto("Honda","CTR")

miMoto.estado()

class Bici(Vehiculos):
    pass

miBici=Bici("Benoto","x-150")

miBici.estado()