class Persona():
    def __init__(self, nombre, edad, lugar):
        self.nombre=nombre
        self.edad=edad
        self.lugar=lugar

    def descripcion(self):

        print("Nombre:",self.nombre,"Edad:",self.edad,"Lugar",self.lugar)

class Empleado(Persona):
    def __init__(self, salario, antigüedad, nombre_empleado, edad_empleado, lugar_empleado):
        super().__init__(nombre_empleado, edad_empleado, lugar_empleado) #Super toma las propiedades del primer constructor
        self.salario=salario
        self.antigüedad=antigüedad

    def descripcion(self):
        super().descripcion()
        print("Salario:", self.salario, "Antigúedad:", self.antigüedad)


Alguien=Empleado(1500, 15, "Carlos", 28, "Colombia")
Alguien.descripcion()

print(isinstance(Alguien,Empleado)) #Nos muestra si la instacia pertenece a un constructor
print(isinstance(Alguien,Persona))