class Vehiculos():
    def __init__(self, marca, modelo):
        self.marca=marca
        self.modelo=modelo
        self.enmarcha=False
        self.acelera=False
        self.frena=False

    def arrancar(self):
        self.enmarcha=True

    def acelerar(self):
        self.acelera=True

    def frenar(self):
        self.frena=True

    def estado(self):
        print("<<------------------>>")
        print(" Marca:",self.marca,"\n Modelo:",self.modelo,"\n En marcha",self.enmarcha,"\n Acelera:",
        self.acelera,"\n Frena:",self.frena)

class Moto(Vehiculos):
    hcaballito=""
    def caballito(self):
        self.hcaballito="Vamo haciendo el caballito"
    
    def estado(self):
        print("<<---Sobrescrito--->>")
        print(" Marca:",self.marca,"\n Modelo:",self.modelo,"\n En marcha",self.enmarcha,"\n Acelera:",
        self.acelera,"\n Frena:",self.frena,"\n Acción añadida:",self.hcaballito)

miMoto=Moto("Honda","CTR")
miMoto.caballito() #Cuando se añade una nueva propiedad va primero
miMoto.estado()

class Furgoneta(Vehiculos):
    def carga(self,cargar):
        self.cargado=cargar
        if(self.cargado):
            return "La furgoneta esta cargada"
        else:
            return "La furgoneta no esta cargada"

miFurgoneta=Furgoneta("Renaut","Kangoo")
miFurgoneta.arrancar()
miFurgoneta.estado()
print(miFurgoneta.carga(True))


class VElectricos(Vehiculos):
    def __init__(self, autonomia, nombre_marca, nombre_modelo):
        super().__init__(nombre_marca, nombre_modelo)
        self.autonomia=autonomia
    
    def estado(self):
        super().estado()
        print("Autonomia",self.autonomia)

    def cargarEnergia(self):
        self.cargando=True

bici_1=VElectricos("Autonomo","Rex-1","002-s")
bici_1.estado()

class BiciElectrica(VElectricos,Vehiculos): #Herencia multiple, Se le da preferencia a la primera clase y toma ese constructor
    pass

miBici=BiciElectrica("Hola","Benoto","Space-007")