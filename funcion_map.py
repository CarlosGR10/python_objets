# -*- coding: utf-8 -*-
"""
Created on Tue Nov  5 21:13:50 2019

@author: Carlos García
"""

#Funcion Map

''' Aplica a una función a cada elemento de una lista interable(listas,tuples, etc)
devolviendo una lista con los resueltos'''

class Empleado:
    
    def __init__(self,nombre,cargo,salario):
        
        self.nombre = nombre
        
        self.cargo = cargo
        
        self.salario = salario
        
    def __str__(self):
        
        return "{} que trabaja como {} tiene un salario de {} $".format(self.nombre,self.cargo,self.salario)
        
        
listaEmpleados = [
        
        Empleado("Juan","Director",2500),
        Empleado("Pedro","Presidente",8200),
        Empleado("Ana","Administrativo",1800),
        Empleado("Pablo","Secretario",1500),
        Empleado("Victor","Ingeniero",2000),
]

def calculo_utilidad(trabajador):
    
    if(trabajador.salario<=2000): #A los menores de 2000
    
        trabajador.salario = trabajador.salario * 1.03
    
    return trabajador

listaTrabajadores=map(calculo_utilidad,listaEmpleados)

for tbr in listaTrabajadores:
    print(tbr)
    
    

